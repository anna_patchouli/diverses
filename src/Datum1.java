
public class Datum1 implements Ausgabetypen{
	private int tag, monat,jahr;
	
	public Datum1(int tag, int monat, int jahr) {
		this.tag = tag;
		this.monat = monat;
		this.jahr = jahr;
	}
	
	public Datum1() {
		this(1,1,2000);
	}
	
	
	@Override
	public void ausgabe() {
		// TODO Auto-generated method stub
		System.out.println("Datum: "+ tag +"." +monat+ "." +jahr);
	}

	@Override
	public void ausgabeLang() {
		// TODO Auto-generated method stub
		System.out.println("Das gespeicherte Datum ist der "+ tag +"." +monat+ "." +jahr);
	}

	@Override
	public void ausgabeKurz() {
		// TODO Auto-generated method stub
		System.out.println( tag +"." +monat+ "." +jahr);
		
	}

}
