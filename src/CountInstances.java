
public class CountInstances {
	
	private static int numInstances = 0;
	
	protected static int getNumInstances() {  // kann von abgeleiteten Klasse verwendet werden
		return numInstances;
	}

	private static void addInstance() {
		numInstances++;
	}
	
	CountInstances(){     //Konstruktor
		CountInstances.addInstance();
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Start with  " + CountInstances.getNumInstances() + " instaces");
		for (int i = 0; i < 10; ++i) 
			new CountInstances(); // kann ohne Bezeichner im Speicher erzeugt werden
		System.out.println("Created  " + CountInstances.getNumInstances() + " instaces");

	}

}
